--[[

	* �����������: Theme Cole aka anticheat.
	* ������: v0.1m
	
	* �������� ���: Monser DeathMatch Two
	* IP-�����: 194.93.2.191:7777

--]]

local inicfg = require 'inicfg'
local encoding = require 'encoding'
local imgui = require 'imgui'
local bNotf, notf = pcall(import, "imgui_notf.lua")

mainIni = inicfg.load(
{
	config =
	{
		posX = 17,
		posY = 2,
	}
}, 	"LogoCfg")

local status = inicfg.load(mainIni, 'LogoCfg.ini')
if not doesFileExist('moonloader/config/LogoCfg.ini') then inicfg.save(mainIni, 'LogoCfg.ini') end

logotype_active = false
loogtype_pos = false

function main()
	if not isSampLoaded() or not isSampfuncsLoaded() then return end
	while not isSampAvailable() do wait(100) end
	sampRegisterChatCommand("logo", logo_active)
	sampRegisterChatCommand("logo_pos", logo_pos)
    if bNotf then notf.addNotification('����������� �� Custom Monser\n\n���������/�����������: /logo\n�������� �������: /logo_pos\n\n����� �������: anticheat.', 5, 1) end

	local texture_monser = renderLoadTextureFromFile(getGameDirectory() .. '\\moonloader\\images\\logoa.png')

	while true do 
		wait(0)
    	if logotype_pos then
            local int_posX, int_posY = getCursorPos()
            
            mainIni.config.posX = int_posX + -200
            mainIni.config.posY = int_posY + -70

            if isKeyJustPressed(13) then
                showCursor(false, false)
                if bNotf then notf.addNotification('����������� �� Custom Monser\n\n�� ������� ���������� ����� �������\n\nX-Pos: ' .. mainIni.config.posX .. '\nY-Pos: ' .. mainIni.config.posY, 5, 1) end
                logotype_pos = false
                inicfg.save(mainIni, 'LogoCfg.ini')
            end
        end
		if logotype_active then
			for i = 60, 86 do sampTextdrawDelete(i) end
			renderDrawTexture(texture_monser, mainIni.config.posX, mainIni.config.posY, 200, 85, 0, 0xFFFFFFFF)
		end	
	end
end

function logo_active()
	logotype_active = not logotype_active
    if bNotf then notf.addNotification('����������� �� Custom Monser:\n\n�� ������� ' .. (logotype_active and '������������' or '��������������').. ' ������', 5, (logotype_active and 2 or 3))end
end

function logo_pos()
	showCursor(true, true)
	logotype_active = true
	logotype_pos = true
	if bNotf then notf.addNotification('����������� �� Custom Monser\n\n��� ���������� �������, ������� \'ENTER\'', 7, 3) end
end
